﻿namespace MabiRichPres.Models
{
    class MabiRichPresenceModel
    {

        #region Properties

        public string State { get; set; }
        public string Details { get; set; }
        public string LargeImageKey { get; set; }
        public string LargeImageText { get; set; }
        public string SmallImageKey { get; set; }
        public string SmallImageText { get; set; }

        #endregion

    }
}
