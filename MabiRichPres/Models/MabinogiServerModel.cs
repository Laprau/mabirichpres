﻿namespace MabiRichPres.Models
{
    class MabinogiServerModel
    {

        #region Constructor

        public MabinogiServerModel(string server, string channel)
        {
            Channel = channel;
            Server = server;
        }

        #endregion

        #region Properties

        public string Channel { get; set; }
        public string Server { get; set; }

        #endregion

    }
}
