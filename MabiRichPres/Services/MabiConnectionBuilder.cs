﻿using MabiRichPres.Models;
using MabiRichPres.Properties;
using System.Collections.Generic;
using System.Net.NetworkInformation;

namespace MabiRichPres.Services
{
    class MabiConnectionBuilder
    {

        #region Constructor

        public MabiConnectionBuilder()
        {
            // TODO: make this into an XML in the future, highly unlikely the server IP and ports will change but can never know
            MabiConnectionMap = new Dictionary<string, MabinogiServerModel>
            {
                {"208.85.109.41:11020", new MabinogiServerModel(Resources.Server_Mari, Resources.Channel_1)},
                {"208.85.109.41:11021", new MabinogiServerModel(Resources.Server_Mari, Resources.Channel_2)},
                {"208.85.109.41:11022", new MabinogiServerModel(Resources.Server_Mari, Resources.Channel_3)},
                {"208.85.109.42:11020", new MabinogiServerModel(Resources.Server_Mari, Resources.Channel_4)},
                {"208.85.109.42:11021", new MabinogiServerModel(Resources.Server_Mari, Resources.Channel_5)},
                {"208.85.109.42:11022", new MabinogiServerModel(Resources.Server_Mari, Resources.Channel_6)},
                {"208.85.109.42:11023", new MabinogiServerModel(Resources.Server_Mari, Resources.Channel_7)},
                {"208.85.109.41:11023", new MabinogiServerModel(Resources.Server_Mari, Resources.Channel_Housing)},

                {"208.85.109.43:11020", new MabinogiServerModel(Resources.Server_Ruairi, Resources.Channel_1)},
                {"208.85.109.43:11021", new MabinogiServerModel(Resources.Server_Ruairi, Resources.Channel_2)},
                {"208.85.109.43:11022", new MabinogiServerModel(Resources.Server_Ruairi, Resources.Channel_3)},
                {"208.85.109.44:11020", new MabinogiServerModel(Resources.Server_Ruairi, Resources.Channel_4)},
                {"208.85.109.44:11021", new MabinogiServerModel(Resources.Server_Ruairi, Resources.Channel_5)},
                {"208.85.109.44:11022", new MabinogiServerModel(Resources.Server_Ruairi, Resources.Channel_6)},
                {"208.85.109.44:11023", new MabinogiServerModel(Resources.Server_Ruairi, Resources.Channel_7)},
                {"208.85.109.43:11023", new MabinogiServerModel(Resources.Server_Ruairi, Resources.Channel_Housing)},

                {"208.85.109.45:11020", new MabinogiServerModel(Resources.Server_Tarlach, Resources.Channel_1)},
                {"208.85.109.45:11021", new MabinogiServerModel(Resources.Server_Tarlach, Resources.Channel_2)},
                {"208.85.109.45:11022", new MabinogiServerModel(Resources.Server_Tarlach, Resources.Channel_3)},
                {"208.85.109.46:11020", new MabinogiServerModel(Resources.Server_Tarlach, Resources.Channel_4)},
                {"208.85.109.46:11021", new MabinogiServerModel(Resources.Server_Tarlach, Resources.Channel_5)},
                {"208.85.109.46:11022", new MabinogiServerModel(Resources.Server_Tarlach, Resources.Channel_6)},
                {"208.85.109.46:11023", new MabinogiServerModel(Resources.Server_Tarlach, Resources.Channel_7)},
                {"208.85.109.45:11023", new MabinogiServerModel(Resources.Server_Tarlach, Resources.Channel_Housing)},

                {"208.85.109.47:11020", new MabinogiServerModel(Resources.Server_Alexina, Resources.Channel_1)},
                {"208.85.109.47:11021", new MabinogiServerModel(Resources.Server_Alexina, Resources.Channel_2)},
                {"208.85.109.47:11022", new MabinogiServerModel(Resources.Server_Alexina, Resources.Channel_3)},
                {"208.85.109.48:11020", new MabinogiServerModel(Resources.Server_Alexina, Resources.Channel_4)},
                {"208.85.109.48:11021", new MabinogiServerModel(Resources.Server_Alexina, Resources.Channel_5)},
                {"208.85.109.48:11022", new MabinogiServerModel(Resources.Server_Alexina, Resources.Channel_6)},
                {"208.85.109.48:11023", new MabinogiServerModel(Resources.Server_Alexina, Resources.Channel_7)},
                {"208.85.109.47:11023", new MabinogiServerModel(Resources.Server_Alexina, Resources.Channel_Housing)},

            };

            RichPresenceModel = new MabiRichPresenceModel();
            RichPresenceModel.LargeImageKey = Resources.Mabinogi_LargeImageKey;
            RichPresenceModel.LargeImageText = Resources.Mabinogi_LargeImageText;
        }

        #endregion

        #region Properties

        private Dictionary<string, MabinogiServerModel> MabiConnectionMap { get; set; }
        private MabiRichPresenceModel RichPresenceModel { get; set; }
        private MabinogiServerModel ServerModel { get; set; }

        #endregion

        #region Public

        public MabiRichPresenceModel GetCurrentRichpresenceModel()
        {
            UpdateServerModel();
            return RichPresenceModel;
        }

        #endregion

        #region Private Helpers

        private void UpdateServerModel()
        {
            CheckCurrentServer();

            // not connected
            if (ServerModel == null)
            {
                RichPresenceModel.Details = "Connecting";
                RichPresenceModel.State = "";
                RichPresenceModel.SmallImageKey = "";
                RichPresenceModel.SmallImageText = "";
                return;
            }

            RichPresenceModel.Details = ServerModel.Server;
            RichPresenceModel.SmallImageText = ServerModel.Server;
            RichPresenceModel.SmallImageKey = ServerModel.Server.ToLower();
            RichPresenceModel.State = ServerModel.Channel;
        }

        private void CheckCurrentServer()
        {
            List<TcpConnectionInformation> connectionInformation = ConnectionListener.ShowActiveTcpConnections();


            foreach (TcpConnectionInformation connection in connectionInformation)
            {
                // recently disconnected channels will register as TIME WAIT
                if (connection.State == TcpState.TimeWait) { continue; }

                MabinogiServerModel serverModel;
                if (MabiConnectionMap.TryGetValue(connection.RemoteEndPoint.ToString(), out serverModel))
                {
                    ServerModel = serverModel;
                    return;
                }
            }

            ServerModel = null;
        }

        #endregion

    }
}
