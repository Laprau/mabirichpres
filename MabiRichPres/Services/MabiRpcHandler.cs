﻿using MabiRichPres.Logging;
using MabiRichPres.Models;
using MabiRichPres.Properties;
using System.Threading;

namespace MabiRichPres.Services
{
    class MabiRpcHandler
    {

        #region Constructor

        public MabiRpcHandler(string appId)
        {
            AppId = appId;
            ProcessObserver = new MabiProcessWatcher();
            ConsoleLogger = new ConsoleLogger();
            PresenceModel = new MabiRichPresenceModel();
            ConnectionBuilder = new MabiConnectionBuilder();
        }

        #endregion

        #region Properties

        private ConsoleLogger ConsoleLogger { get; set; }
        private MabiProcessWatcher ProcessObserver { get; set; }
        private MabiRichPresenceModel PresenceModel { get; set; }
        private MabiConnectionBuilder ConnectionBuilder { get; set; }


        public string AppId { get; set; }

        #endregion

        #region Public

        public void Run()
        {
            ConsoleLogger.Log("Starting Event Handlers...");

            DiscordRpc.EventHandlers eventHandlers = new DiscordRpc.EventHandlers();

            ConsoleLogger.Log("Initializing Discord RPC...");
            DiscordRpc.Initialize(AppId, ref eventHandlers, true, null);

            DiscordRpc.RichPresence richPresence = new DiscordRpc.RichPresence();

            while (ConfirmMabinogiStillRunning())
            {
                PresenceModel = ConnectionBuilder.GetCurrentRichpresenceModel();

                UpdatePresence(ref richPresence);

                DiscordRpc.UpdatePresence(ref richPresence);

                Thread.Sleep(1000);
            }

            DiscordRpc.Shutdown();
        }

        #endregion

        #region Private Helpers

        private void UpdatePresence(ref DiscordRpc.RichPresence richPresence)
        {
            richPresence.details = PresenceModel.Details;
            richPresence.state = PresenceModel.State;
            richPresence.largeImageKey = PresenceModel.LargeImageKey;
            richPresence.largeImageText = PresenceModel.LargeImageText;
            richPresence.smallImageKey = PresenceModel.SmallImageKey;
            richPresence.smallImageText = PresenceModel.SmallImageText;
        }

        private bool ConfirmMabinogiStillRunning()
        {
            if (!ProcessObserver.IsMabiRunning)
            {
                ConsoleLogger.Log(Resources.Mabi_Stopped, true);
            }

            return ProcessObserver.IsMabiRunning;
        }

        #endregion

    }
}
