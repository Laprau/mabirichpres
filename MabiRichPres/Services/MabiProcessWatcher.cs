﻿using System.Diagnostics;

namespace MabiRichPres.Services
{
    class MabiProcessWatcher
    {
        #region Properties

        public bool IsMabiRunning { get { return MabinogiIsRunning(); } }

        #endregion

        #region Private Helpers

        // TODO: Could probably just be static; would consider adding more functionality in the future
        private bool MabinogiIsRunning()
        {
            foreach (Process process in Process.GetProcesses())
            {
                if (process.MainWindowTitle == "Mabinogi")
                {
                    return true;
                }
            }

            return false;
        }

        #endregion
    }
}
