﻿using System.Collections.Generic;
using System.Net.NetworkInformation;

namespace MabiRichPres.Services
{
    class ConnectionListener
    {

        #region Static

        public static List<TcpConnectionInformation> ShowActiveTcpConnections()
        {
            IPGlobalProperties properties = IPGlobalProperties.GetIPGlobalProperties();
            TcpConnectionInformation[] connections = properties.GetActiveTcpConnections();
            return new List<TcpConnectionInformation>(connections);
        }

        #endregion

    }
}
