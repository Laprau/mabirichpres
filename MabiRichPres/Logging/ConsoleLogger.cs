﻿using System;

namespace MabiRichPres.Logging
{
    class ConsoleLogger
    {

        #region Public

        public void Log(string logMessage, bool waitforinput = false)
        {
            string message = string.Format("{0} | {1}", DateTime.Now.ToString("HH:mm:ss"), logMessage);
            Console.WriteLine(message);

            if (waitforinput)
            {
                Console.ReadKey();
            }
        }

        #endregion

    }
}
