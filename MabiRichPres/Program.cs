﻿using MabiRichPres.Properties;
using MabiRichPres.Services;
using System;

namespace MabiRichPres
{
    class MabiRichPresIntegrator
    {
        // TODO: Config in the future
        #region Entry Point

        private static void Main()
        {
            Console.CancelKeyPress += delegate
            {
                DiscordRpc.Shutdown();
            };

            MabiRpcHandler mabiRpcHandler = new MabiRpcHandler(Resources.Application_Id);
            mabiRpcHandler.Run();
        }

        #endregion

    }
}
