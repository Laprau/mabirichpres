# Mabinogi Rich Presence Integration

I know what you're thinking, and no, this is __NOT__ a mod for Mabinogi.

## What does it do?

This is simply an application made to run alongside Mabinogi in order to have that boring Discord presence change into a rich presence in the least intrusive way possible.

__It allows you to go from this__

![Non-Rich Presence](images/nonrich.png)

__to this__

![Rich Presence](images/richpres.png)

# How do I use it?

1. Download the latest release zip file from
2. Unpack the zip file into a location of your choosing
3. Start Mabinogi
4. Navigate to where you extracted the zip file, and run the Mabinogi Rich Presence Client
5. Check Discord; As soon as you log into a channel, your presence should be updated (and will continue to update every second)

## How does it work?

In order to avoid having this classified as a mod, this application does not interact with Mabinogi in any way. 
Instead, this application will look at your open network connections, checking to see if there are any IPs and Ports associated with Mabinogi's servers and channels, and adjust your presence accordingly.

# Future Plans

As this project was a tad rushed (I wanted to have it done in a weekend), the code is not very clean, so there will be an ongoing effort to clean it up.